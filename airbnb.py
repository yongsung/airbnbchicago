#-*-coding:utf-8-*-
import urllib2
from bs4 import BeautifulSoup
from os import listdir
import MySQLdb
import json

# conn = MySQLdb.connect(host='localhost',
#                         user='root',
#                         passwd='1234',
#                         db='airbnb',
#                         charset='utf8',
#                         use_unicode=True)

airbnb_file = file("airbnb_final_3.csv","w")
airbnb_file.write("user, url, property type, room type, ratings, reviews, daily price, weekly price, monthly price, response rate, response time, references, verifiedId, accuracy, communication, cleanliness, location, check in, value, lat, lng, membersince")
# airbnb_file.write("\n")
# cur = conn.cursor()
# cur.execute("DROP TABLE IF EXISTS airbnb")
# cur.execute("CREATE TABLE airbnb(Id INT PRIMARY KEY AUTO_INCREMENT, \
#                  User VARCHAR(25), Url VARCHAR(50), Lat VARCHAR(50), Lng VARCHAR(50))")
for i in range(1,56):
    url_str = "https://www.airbnb.com/s/Chicago--IL?ss_id=83ne76sj&page=%s" % i
    # print url_str
    aribnbURL = urllib2.urlopen(url_str)
    html = aribnbURL.read()
    tmp_soup = BeautifulSoup(html)
    print "page number: " + str(i)
    rooms = tmp_soup.find("div", attrs={"class": "listings-container"}).findAll("div", attrs={"class":"listing"})
    for room in rooms:
        try:
            user = room.attrs["data-user"]
            url = "http://www.airbnb.com" + room.attrs["data-url"]
            room_type = room.attrs["data-name"]
            lat = room.attrs["data-lat"]
            lng = room.attrs["data-lng"]
            print "user: " + user
            print "url: " + url
    #parsing individual page
            airbnbURL = urllib2.urlopen(url)
            airbnb_html = airbnbURL.read()
            soup = BeautifulSoup(airbnb_html)
            accuracy = ""
            cleanliness = ""
            checkin = ""
            communication = ""
            location = ""
            value_rating = ""
            room_type = ""

            room_options = soup.find("meta", attrs={"id": "_bootstrap-room_options"}).attrs["content"]
            if(room_options):
                dict_room = json.loads(room_options)
                if (dict_room["airEventData"]["accuracy_rating"]):
                    accuracy = str(dict_room["airEventData"]["accuracy_rating"])
                    cleanliness = str(dict_room["airEventData"]["cleanliness_rating"])
                    checkin = str(dict_room["airEventData"]["checkin_rating"])
                    communication = str(dict_room["airEventData"]["communication_rating"])
                    location = str(dict_room["airEventData"]["location_rating"])
                    value_rating = str(dict_room["airEventData"]["value_rating"])
                    room_type = str(dict_room["airEventData"]["room_type"])
                    print accuracy, cleanliness, checkin, communication, location, value_rating

            monthly_price = soup.find("span", attrs={"id": "monthly_price_string"}).contents[0] if soup.find("span", attrs={"id": "monthly_price_string"}) else ""
            weekly_price = soup.find("span", attrs={"id": "weekly_price_string"}).contents[0] if soup.find("span", attrs={"id": "weekly_price_string"}) else ""
            print "monthly price" + ": " + monthly_price
            print "weekly price" + ": " + weekly_price
            ratings = soup.find("meta", attrs={"itemprop": "ratingValue"}).attrs["content"] if soup.find("meta", attrs={"itemprop": "ratingValue"}) else ""

            reviews = soup.find("span", attrs={"itemprop": "reviewCount"}).contents[0] if soup.find("span", attrs={"itemprop": "reviewCount"}) else ""
            # metacontent = soup.find("meta")
            res_rate = soup.find("small", attrs={"class": "response-details"}).parent.find("strong").contents[0] if soup.find("small", attrs={"class": "response-details"}) else ""
            res_time = ""
            if(soup.find("small", attrs={"class": "response-details"})):
                print res_rate.parent.contents[0].strip() + ": " + res_rate
                res_time = soup.find("small", attrs={"class": "response-details"}).parent.findNext("div").find("strong").contents[0]
                print res_time

            references = soup.find(text="References").parent.parent.find("span").contents[0] if soup.find(text="References") else ""

            print "references: " + references


            verified = soup.find(text="Verified ID") if soup.find(text="Verified ID") else ""
            if(verified):
                print verified
            # reviews = soup.find(text="Reviews") if soup.find(text="Reviews") else ""
            # if(reviews):
            #     print reviews + ": " + reviews.parent.parent.find("span").contents[0]
            price = soup.find("div", attrs={"itemprop": "price"}).contents[0].strip() if soup.find("div", attrs={"itemprop": "price"}) else ""

            print "price : " + price
            # print review.contents[0].strip()

            basicInfo = soup.find("div", attrs={"id": "details"}).find("div", attrs={"class": "row"})
            prop = basicInfo.findAll("div", attrs={"class": "col-6"})[0].findAll("strong")

            property_type =""
            for i in prop:
                if i.previousSibling == "Property type: ":
                    print "property type: " + i.contents[0].contents[0]
                    property_type = i.contents[0].contents[0]
            print "room type: " + room_type
            member_since = soup.find("small", attrs={"class": "response-details"}).parent.parent.parent.find("div").findAll("div")[1].contents[0].strip().split("Member since ")[1]
            print "member since: " + member_since
            print "\n"

            # print user + ", " + url + ", " + property_type + ", " + ratings + ", " + reviews + ", " + price + ", " + weekly_price + ", " + monthly_price + ", " + res_rate + ", " + res_time + ", " + references + ", " + verified + ", " + lat + ", " + lng

            room_str = user + ", " + url + ", " + property_type + ", " + room_type + ", " + ratings + ", " + reviews + ", " + price + ", " + weekly_price + ", " + monthly_price + ", " + res_rate + ", " + res_time + ", " + references + ", " + verified + ", " + accuracy + ", " + communication + ", " + cleanliness + ", " + location + ", " + checkin + ", " + value_rating + ", " + lat + ", " + lng + ", " + member_since
            # # # # user, url, property type, ratings, reviews, daily price, weekly price, monthly price, response rate, response time, references, verifiedId, accuracy, communication, cleanliness, location, check in, value, lat, lng
            airbnb_file.write(room_str)
            airbnb_file.write("\n")
        except Exception, e:
            f_error = file("error.txt", "a")
            f_error.write(str(i) + "\t" + str(e) + "\n" )
            f_error.close()
airbnb_file.close()


        # sql = "INSERT INTO airbnb (User, Url, Lat, Lng) VALUES ('%s', '%s', '%s', '%s')" % (user, url, lat, lng)
        # print sql
        # cur.execute(sql)
        # conn.commit()


##################property#####################
# reviewCount = soup.find("span", attrs={"itemprop": "reviewCount"}).contents[0]


# for i in basicInfo:
#     j = i.findAll("div",attrs={"class": "col-6"})
#     for x in j:
#         print x.find("strong")
#         print x.find("strong").parent.contents[0]
#         break



#     html = aribnbURL.read()
#     soup = BeautifulSoup(html)

